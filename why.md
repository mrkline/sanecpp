---
layout: page
title: Why do we need another C++ tutorial?
---

C++ scares people.
Far too often, they see it as extremely troublesome,
worth it only when you're desperate for every last cycle of performance.
Even in those cases, some argue its maze of features hurt more than help,
so you're better off retreating to good old C.

Many existing resources, from online tutorials to courses at great universities,
only make matters worse.
Since C++ has historical roots in C,
they insist on a "C first" teaching strategy, starting with a crash course in C
before explaining the differences between the two languages.
But good C doesn't make good C++, and students are overwhelmed by concepts like:

- Raw arrays (and how they decay to pointers)
- Pointer math
- `printf` and all its wonderful formatting codes
- Manual memory management

and so on.
While these are all good topics to cover _in due time_, they do little more than
flood the beginner with information they don't immediately need.
Follow it up with a long lineup of C++ features without good examples of how
they're useful, and it's no wonder when students are left dazed and confused.

It shouldn't be this bad.
[Recent](https://en.wikipedia.org/wiki/C%2B%2B11)
[standards](https://en.wikipedia.org/wiki/C%2B%2B14)) have made tremendous
strides in simplicity of use.
C++ is enjoying a second renaissance, and while it certainly has its warts,
it isn't nearly as scary as some would have you believe.
By teaching modern principles, introducing topics in a more natural order,
and not getting mired in the past, _**Sane C++**_ aims to be a clear and
straightforward way to learn the language.
