---
layout: page
title: Setting up your system
---

One major advantage of C++'s popularity and longevity are the
excellent tools that have developed around it.
Like tools for many other languages, they generally fall into two main camps:

## Integrated Development Environments (IDEs)
These are big, graphical programs designed to be a one-stop shop.
They usually include an editor, compiler, debugger, and several other
tools, all in one package. Popular C++ IDEs include:

- [Visual Studio (Windows)](https://www.visualstudio.com/vs/)
- [Visual Studio Code (Windows, Linux, Mac)](https://code.visualstudio.com/)
- [XCode (Mac)](https://developer.apple.com/xcode/)
- [Code::Blocks (Windows, Linux, Mac)](http://www.codeblocks.org/)
- [JetBrains CLion (Windows, Linux, Mac)](https://www.jetbrains.com/clion/)

This tutorial won't provide examples for IDEs since each of them behave a bit
differently.
You can certainly follow along using an IDE, but you will be on your own
when it comes to setting up a project, building it, and testing it.
If you choose that path, good luck and happy Googling.

## Command-line tools

Before the days of computer graphics, programs communicated in text.
You typed commands, and they printed out their output.
While this might seem ancient and backwards to some computer users today,
[command-line interfaces (CLIs)](https://en.wikipedia.org/wiki/Command-line_interface)
live on, and even thrive, among programmers.
Though they can take some getting used to, you'll find that they can be just
as powerful and useful as graphical tools (or even more so!).

Two great, open-source (and free) CLI compilers dominate all others.
Many of the IDEs above use them internally.

1. [**GCC**](https://gcc.gnu.org/) was originally released as a C compiler
   in 1987 by [Richard Stallman](https://en.wikipedia.org/wiki/Richard_Stallman).
   In the years since, it has grown to support many different languages
   (including C++) and has become one of the most important and widespread
   pieces of open-source software.
   It is part of the [GNU Project](https://en.wikipedia.org/wiki/GNU_Project),
   whose ongoing mission is to develop [free software](http://www.gnu.org/philosophy/free-sw.en.html)
   so that users have the freedoms to use, share, study, and modify the
   programs that run on their computers.

2. [**Clang**](http://clang.llvm.org/) and the related
   [LLVM](http://www.llvm.org/) project are much more recent
   open-source ventures, driven by Apple, Google, ARM, Sony, and Intel.
   Along with its compiler, Clang provides libraries
   for building other C++ tools, like ones for
   code completion, syntax highlighting, and refactoring.

This tutorial will provide examples using Clang's C++ compiler, clang++.
However, the options for GCC's C++ compiler, g++, are mostly the same.
Either one will do.

### How do I install GCC or Clang on...

#### Linux?

Linux, being a descendent of [Unix](https://en.wikipedia.org/wiki/Unix),
has a rich ecosystem of command line tools.
Depending on your distribution, there's a good chance you already have a C++
compiler installed.
Open a terminal and run `gcc --version`.
If that doesn't work, you'll have to install one---and its related tools---from
your package manager.

For Ubuntu and other Debian-based systems, run

    sudo apt-get install clang

for Clang, or

    sudo apt-get install build-essential

for GCC.
For some other systems (Arch, OpenSUSE, Fedora, etc.), look online if you're
not familiar with your system's package manager.
It's a good tool to know, at any rate.

#### Mac?

With Apple being one of the main drivers of Clang and LLVM,
you'll find easy access on OS X.
Install [XCode](https://developer.apple.com/xcode/)
and the command line tools that come with.
See [here](https://developer.apple.com/library/ios/technotes/tn2339/_index.html)
for more info.

#### Windows?

Several different distributions of Unix tools exist for Windows, including
[MSYS2](https://msys2.github.io/),
its predecessor [MSYS](http://www.mingw.org/wiki/msys),
and [Cygwin](https://www.cygwin.com/).
See their websites for help on downloading them and getting them set up.

## Editors

If you're not using an IDE, you'll need to write your code with a text editor.
If you don't have a preferred one already, here are some popular ones,
listed in no particular order.
Try a few and find one that works for you---programmers have been arguing over
which one is the best for longer than I've been alive.

- [**Sublime Text**](http://www.sublimetext.com/) is a closed-source
  editor, popular for its many shortcuts, commands, and plugins.
  It isn't free---neither as in _freedom_ nor as in _free cake_---but the unpaid
  version is identical to the paid one except for occasional pop-ups asking
  you to buy it.

- [**Atom**](https://atom.io/) is an editor by Github, built on web technologies
  (HTML, JavaScript, and CSS).
  It has its own package manager for [plugins](https://atom.io/packages),
  making finding and installing them easy.

- [**Vim**](http://www.vim.org/)
  is an old and well-respected editor whose roots can be traced back to 1976.
  What makes it different from many other text editors is its several
  different modes.
  Along with "insert" mode, where you type text like you would in other editors,
  its other modes allow you to issue commands which transform your text.
  It has a steep learning curve, but once you understand some of the command
  ["language"](http://yanpritzker.com/2011/12/16/learn-to-speak-vim-verbs-nouns-and-modifiers/),
  you can modify code (and any other text) quickly and easily.
  It is also scriptable, and has many plugins available online.
  Vim can run in your terminal as a command line program, or as a GUI.
  There's also a popular fork, [**Neovim**](https://neovim.io/), which offers
  features like a built-in terminal emulator.

- [**Emacs**](https://www.gnu.org/software/emacs/),
  like Vim, dates back to the late 1970s and remains popular today.
  It contains a built-in environment for the
  [Lisp](https://en.wikipedia.org/wiki/Emacs_Lisp) programming language,
  making it incredibly flexible.
  It isn't uncommon for Emacs users to keep notes, manage their calendar,
  and even read and send emails from within the editor.
  Like Vim, Emacs can run as both a command line program and as a GUI.

- [**Nano**](https://www.nano-editor.org/)
  is a simple command line editor.
  It doesn't have as many features as any of the other choices,
  but is very simple to learn and use.

{% include next.md title="Hello, World!" url="hello" %}
