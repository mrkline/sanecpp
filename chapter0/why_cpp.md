---
layout: page
title: Why should I learn C++?
---

Right now is a better time than any to learn a new programming language.
Dozens of popular ones exist in the wild,
each with solid free and open-source tooling, massive online communities,
and more resources for beginners than ever before.

C++ isn't exactly known for being beginner friendly, and it has more than its
fair share of [well-known and vocal critics](http://harmful.cat-v.org/software/c++/linus).
So why should you bother learning it?

- **It's everywhere.** If you're reading this in Chrome, Firefox, or Safari,
  these pixels were put in front of your face using C++.
  If you're on Windows or Mac OS, your operating system is built with it.
  Thousands of embedded devices, some of which you probably use every day,
  have firmware written in it.
  Almost every single video game you have ever played was made with it.
  You can find programmers writing C++ in nearly every corner of the
  software industry and the open-source community.

- **It's better than ever.** After lying largely unchanged for a decade,
  new language standards [in 2011](https://en.wikipedia.org/wiki/C%2B%2B11)
  and [2014](https://en.wikipedia.org/wiki/C%2B%2B14) have made C++
  more expressive and easier to use, addressing long-standing complaints.

- **It's not going anywhere.**
  C++ competes in many arenas with its forefather, C, and new languages like
  [D](http://dlang.org/) and [Rust](https://www.rust-lang.org/).
  But C++ use is widespread and shows few signs of disappearing.
  With ongoing work to add new, useful features,
  it remains a solid choice for projects.

- **Performance still matters.**
  Some will claim that performance isn't important anymore due to the rapid
  expansion of computing power in the last few years.
  But in almost every domain, speed still matters.
  In datacenters, an inefficient program is more expensive.
  On your laptop and desktop, an inefficient program worsens your user experience.
  And on your phone or tablet, an inefficient program drains your battery.

    C++ offers you abstractions to express your ideas without
    worrying about the little stuff. But unlike many other languages, C++ lets
    you dive down "to the metal" at the same time, giving you control over
    every bit of your hardware when you need to go as fast as possible.
    This mix of low and high level is fairly rare in programming,
    and is the main reason you'll see C++ just about everywhere.

{% include next.md title="What does this tutorial cover?" url="what" %}
