---
layout: page
title: This guide is missing something!
---

This guide is meant to be short and succinct, but there is a good chance that
it's missing something or got something wrong, especially in these early days.

If you'd like to help, the sources for this site are available on Github at:

<https://github.com/mrkline/sanecpp>

Pull requests are welcome.

If none of that made sense, you should learn about
[Git](http://git-scm.com/) and contributing via
[pull requests](https://help.github.com/articles/using-pull-requests/).
In the meantime, feel free to send an email to
[contact@sanecpp.com](mailto:contact@sanecpp.com).
I'll try to respond quickly, but please be patient.

{% include main.md %}
