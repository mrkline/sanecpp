---
layout: page
title: Hello, World!
---

Now that you have your compiler installed, let's test it out.
["Hello, World!"](https://en.wikipedia.org/wiki/%22Hello,_World!%22_program)
is the simplest program you can write.
It writes a message and exits, and in C++, it looks something like this:

```c++
#include <iostream>

using namespace std;

// Say hello
int main()
{
    cout << "Hello, World!" << endl;
    return 0;
}
```

Save your file as `hello.cpp`.
To compile it, we'll do the following.

    clang++ -o hello hello.cpp

(For those using GCC, just replace `clang++` with `g++`.)

The `-o` option tells the compiler what to name your program.
If you don't specify a name, the default one is `a.out` for
[historical reasons](https://en.wikipedia.org/wiki/A.out).

Run your program with `./hello` and you should see something like this:

```
$ ./hello
Hello, World!
```

Nicely done! You've just written, compiled, and run your first C++ program.

## So what does this stuff mean?

Let's take a look at what each bit is doing.

```c++
#include <iostream>
```

Like almost every other language, C++ provides a *standard library*,
a set of general-purpose tools for many common programming tasks.
We use `#include`{:.language-cpp} to import code found in other files, including the standard
library.
Here we use `iostream`, which gives us the ability to write text.

```c++
using namespace std;
```

C++ allows you to organize code into named groups, or *namespaces*.
Code from the standard library (such as the `cout` and `endl` below) are in
the namespace `std`.
You could call them by their "full" names, `std::cout` and `std::endl`,
but to save us some typing, this line instructs the compiler to check the
`std` namespace for you. Simple.

```c++
// Say hello
```

Anything on a line following `//` is a comment.
These are completely ignored by the compiler,
but can help explain your code to others---or a future version of yourself!
You can also use `/*` or `*/` to mark a comment's start and end:

```c++
/*
This commment
is several
lines long.
*/
```

This is useful when a comment spans several lines.

```c++
int main()
{
    // Other stuff goes here
    return 0;
}
```

Like many other langauges, C++ groups code into *functions*,
and the operating system starts your program by calling the `main` function.
When a program exits, it provides a
[return code](https://en.wikipedia.org/wiki/Exit_status)
that other programs can examine to check for errors.
Returning zero means, "everything went okay."
You'll also notice that statements in C++ end in semicolons.

```c++
cout << "Hello, World!" << endl;
```

This is the stuff we care about. Everything else is just scaffolding.
`cout` is short for "console output" and we send things to it using `<<`.
`endl` prints a newline character to move your console cursor to the next line.

And there we have it!
I'm sure you have some questions about exactly how some of this works.
We'll explain more very soon!

## More compiler options

While we're getting set up, there's a few other compiler options we'll want
to use regularly.
GCC and Clang have an
[insane amount of options](https://gcc.gnu.org/onlinedocs/gcc/Option-Summary.html),
but there's a couple we care about right now:

- `-std=c++14`: After nearly a decade without changes, C++ was updated in 2011,
  then again in 2014.
  Some compilers still default to the older version of the language (C++98),
  but this option tells the compiler to use the 2014 edition (C++14).

- `-Wall`: This turns on a bunch of warnings.
  While having your compiler complain more often might sound annoying,
  warnings are signs that your code isn't doing what you think it's doing.
  You should do your best to make sure your code compiles without any warnings.
  Some even like to use `-Werror`, which turns all warnings into errors.

- `-Wextra`: Oddly enough, `-Wall` doesn't turn on *all* warnings.
  This enables even more.

You should be able to recompile your program with these options turned on:

    clang++ -std=c++14 -Wall -Wextra -o hello hello.cpp

and everything should work just how it did before.

{% include next.md title="This guide is missing something!" url="contributing" %}
