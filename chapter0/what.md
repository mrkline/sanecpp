---
layout: page
title: What does this tutorial cover?
---

This site will not teach you how to be a good programmer.
It will not teach you software design principles.
It won't even teach you to be a great C++ programmer.
These things take years of practice---you can't become an instant master
by reading this tiny website.
We also won't discuss version control, testing, build systems,
or issue tracking.
These are all very important for good software design,
but excellent resources exist for them elsewhere.
Trying to cover them here would be a massive disservice to you, the reader.
Instead, this tutorial focuses on the fundamentals of modern C++,
and little else.

C++ is a complicated language for two main reasons:

1. **Flexibility:** C++ contains many different tools and approaches for
   many different tasks.
   When used well, you can make short work of your problems.
   When misused, you can get an awful mess.
   Moreover,
2. **Legacy:** C++ has been in widespread use for over two decades now.
   Modern C++ (following the [C++11](https://en.wikipedia.org/wiki/C%2B%2B11)
   and [C++14](https://en.wikipedia.org/wiki/C%2B%2B14) standards) is much easier
   to use than it used to be, but the "old ways" are kept around so
   older programs can still be built on new compilers.

By sidestepping legacy complications and minutiae, and by introducing language
features in a natural order, I hope to avoid the usual headaches.
For more info, see [_Why do we need another C++ tutorial?_](/why.html)

This tutorial is aimed at those with some programming experience
who hope to add C++ to their tool belt.
Those brand new to programming should hopefully be able to follow along.

{% include next.md title="Setting up your system" url="setup" %}
