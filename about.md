---
layout: page
title: About Sane C++
---

**Sane C++** is written and maintained by
by <a xmlns:cc="http://creativecommons.org/ns#" href="http://bitbashing.io" property="cc:attributionName">Matt Kline</a>.
Its inspiration comes from
Kate Gregory's [_Stop Teaching C_ talk](https://www.youtube.com/watch?v=YnWhqhNdYyk),
given at [CppCon](http://cppcon.org/) 2015.

Sane C++ is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
