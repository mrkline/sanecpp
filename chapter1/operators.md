---
layout: page
title: Operators
---

In our last lesson, we used the _assignment operator_ to give variables a value:

```cpp
int theAnswer = 42;
```

C++ also has your usual cadre of basic mathematical operators:

```cpp
int sum = a + b; // Add a and b
int difference = c - d; // Subtract c and d
int product = e * f; // Mulitply e and f
int quotient = g / h; // Divide g and h
```

It also provides a modulo operator for integers:

```cpp
int remainder = a % b; // Perform a / b, then take the remainder.
```

There's a few important things to note:

- When dividing two integers, any fractional part of the quotient is discarded.
  This means, for example, that `2 / 3` yields `0`.
  If you care about the fractional part, we must use floating-point values,
  e.g., `2.0 / 3.0`.

- There is no modulo operator for floating-point types.
  To get the remainder when dividing two `float`{:.language-cpp} or
  `double`{:.language-cpp} values, we need to call a
  [standard library function](http://en.cppreference.com/w/cpp/numeric/math/fmod).
  We'll talk more about functions shortly.

## Compound assignments

In many cases, we want to take a variable, perform an operation on it,
and store the result back. C++ gives us a shorthand to do this:

```cpp
sum += a;        // same as sum = sum + a;
difference -= b; // same as difference = difference - b;
product *= c;    // same as product = product * c;
/// ...And so on for the other operators above.
```

If we're adding or subtracting one, we can be even more succinct:

```cpp
++a; // same as a = a + 1;
--b; // same as b = b - 1;
```

{% include extra.md %}

- `++` and `--` can be used inside of other expressions, e.g.

  ```cpp
  int a = 3 * ++b;
  ```

  Placing these operators before the variable modifies it _before_ evaluating
  the rest of the expression. Placing it after modifies the variable _after_
  evaluating the rest of the expression.
  Doing so more than once in the same expression, e.g.

  ```cpp
  int c = 3 * ++d - ++d;
  ```

  is _undefined behavior_ (UB). UB means,
  "The compiler can do literally anything if you try this, so **don't**."

  At any rate, focus on clarity.
  If breaking an expression into several lines makes it easier to read,
  feel free to do so.

- While the operators above should be all you need to manipulate integers
  and floating-point values, others exist for manipulating the bit patterns
  of integers.
  They include `<<` and `>>` for left and right shifts, along with
  `&`, `|`, `^`, and `~` for bitwise AND, OR, XOR, and NOT operations,
  respectively.
  See [Wikipedia's page on bitwise operators](https://en.wikipedia.org/wiki/Bitwise_operation)
  if you aren't familiar with these.

  They aren't needed most of the time, but are invaluable when writing
  low-level code that needs to communicate directly with hardware.
  Be warned that shifting a value by more bits than it contains is
  undefined behavior.
  For details, see Chandler Carruth's
  [CppCon 2016 talk](https://youtu.be/yG1OZ69H_-o?t=24m39s) on UB.


- Dividing one variable by another
  is a fairly slow operation on most modern processors.
  If the denominator is a known, fixed value---e.g., `a / 42`---the compiler
  can often speed up the process using bit shifts (see above).

{% include next.md title="Conditionals" url="conditionals" %}
