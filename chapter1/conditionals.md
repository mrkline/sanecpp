---
layout: page
title: Conditionals
---

Most programs have to make decisions.
Say I'm writing a video game.
Which direction should my player move?
Has my player been hit? If my health is too low enough when I am hit,
have I lost the game?

In C++, decision-making looks like this:

```c++
#include <iostream>

using namespace std;

int main()
{
    cout << "Pick a number:" << endl;
    int firstNumber;
    cin >> firstNumber;

    cout << "Pick another number:" << endl;
    int secondNumber;
    cin >> secondNumber;

    if (firstNumber < secondNumber) {
        cout << "The first number was smaller than the second.";
    }
    else if (firstNumber > secondNumber) {
        cout << "The first number was larger than the second.";
    }
    else {
        cout << "The two numbers are the same!";
    }

    cout << endl;
    return 0;
}
```

To check if some condition is true, we use a _conditional statement:_

```c++
if (condition)
```

and put the code we want to run when the condition is true inside a pair of
braces, i.e., `{ }`, immediately after.
If we want to check another condition whenever the first one isn't true,
we use

```c++
else if (condition)
```

Multiple `else if`{:.language-cpp} statements can follow each other.
Finally, if we want to run some code when _none_ of the the previous
conditions were true, we use `else`{:.language-cpp}.

In the example above, we check if the first number the user provided is smaller
than the second. If that wasn't the case, we see if the first number is larger.
If it wasn't smaller and it wasn't larger, it must be equal, so we use an
`else { }`{:.language-cpp} block to tell the user as much.

## Relational operators

We can compare variables with _relational operators:_

 Syntax | Relation
--------|---------
`a < b` | _a_ &lt; _b_
--------|---------
`a <= b`| _a_ &le; _b_
--------|---------
`a > b` | _a_ &gt; _b_
--------|---------
`a >= b`| _a_ &ge; _b_
--------|---------
`a == b`| _a_ = _b_
--------|---------
`a != b`| _a_ &ne; _b_

It's important to note that _two_ equals signs are needed to check if one
variable has the same value as another.
This is because the language allows you to perform assignments _inside_
a conditional, e.g.,

```cpp
int i;
if ((i = a + b) > 42) {
    ...
}
```

but doing so is needlessly complicated in most cases.

## Logical operators

You can combine different boolean expressions with _logical operators:_

 Syntax | Operation | Meaning
--------|-----------|--------
`a && b`| _a_ AND _b_ | _a_ AND _b_ is true if both _a_ and _b_ are true.
--------|----------|--------
`a || b`| _a_ OR _b_ | _a_ OR _b_ is true if either _a_ or _b_ are true.
--------|----------|--------
  `!a`  | NOT _a_ | If _a_ is true, NOT _a_ is false, and vice versa.

All of these operators can be used to build more complicated expressions:

```cpp
if (activePlayers > 0 && currentPlayerCanMove) {
    // Take a turn.
}
```

### Short-circuiting

Logical operators in C++ _short-circuit._
That is, if the first expression in `a && b` is false,
or the first expression of `a || b` is true,
the second expression is not evaluated.
This is useful in some situations.
For example, consider:

```cpp
if (denominator != 0 && numerator / denominator < 3) {
    ...
}
```

If the denominator is zero, the expression short-circuits, the program
doesn't perform the division (so that we don't divide by zero),
and the whole condition evaluates to false.

{% include extra.md %}

Remember the last lesson, when we talked about [booleans](variables.html#booleans)?
You might have noticed that all of our conditions are Boolean expressions---they
can either be true or false.
It's worth nothing that

```cpp
if (firstNumber < secondNumber) {
  ...
}
```

is exactly the same as

```cpp
bool smaller = firstNumber < secondNumber;
if (smaller) {
  ...
}
```

{% include next.md title="Loops" url="loops" %}
