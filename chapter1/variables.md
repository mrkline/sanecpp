---
layout: page
title: Variables
---

Programs would be pretty useless if they couldn't interact with the
outside world or store information.
Try this out:

```c++
#include <iostream>
#include <string>

using namespace std;

int main()
{
    cout << "What's your name?" << endl;
    string myName;
    cin >> myName;

    cout << "How old are you?" << endl;
    int myAge;
    cin >> myAge;

    cout << "Hi, " << myName << "!" << endl;
    cout << "You're " << myAge << " years old." << endl;
    return 0;
}
```

If you save it as `hi.cpp`, compile it, and run it,
you'll see something like this:

```
$ clang++ -std=c++14 -Wall -Wextra -o hi hi.cpp
$ ./hi
What's your name?
Matt
How old are you?
24
Hi, Matt!
You're 24 years old.
```

Just like `cout` is short for "console out", `cin` is short for "console in",
and is an easy way to get input from a user's terminal.
Here we store our inputs in variables named `myName` and `myAge`.
Let's talk about how variables work in C++.

## Types

C++ is a *statically typed* language.
Each variable stores a certain kind of information,
and if you try to assign it a different kind of value, it won't work.
(It won't even compile!)
To specify the variable's type, and to tell the compiler
when you plan to use it, variables must be *declared*.
The type comes first, then its name, like so:

```c++
int someNumber;
```

You can also give it a starting value:

```c++
int theAnswer = 42;
```

There are a few types you'll use often. Let's go over them.

### Strings

Computers store text as "strings", or series of characters.
As you saw above, the C++ standard library offers a `string` type for just this
purpose. To use it, we should `#include <string>`{:.language-cpp}.

### Integers

[Integers](https://en.wikipedia.org/wiki/Integer) are whole numbers.
In C++, they're stored using a fixed number of bytes
(usually four), so they have a fixed range of values.
Fortunately, the range is large enough we usually don't have to worry about it,
since four bytes gives you values from -2,147,483,648 to 2,147,483,647.
As you saw above, we declare integers in C++ with `int`{:.language-c++}.

"Unsigned" integers are also available, using `unsigned int`{:.language-c++}.
They can't store negative values, but instead store twice the positive range
of their signed counterparts (for four bytes, 0 to 4,294,967,296).
They are useful in a few situations, but you should usually prefer signed ones.

### Floating-point numbers

Sometimes we need values besides whole numbers.
For these situations, we have `double`{:.language-c++}:

```c++
double pi = 3.14159265359;
```

Like `int`{:.language-c++}, `double`{:.language-c++} is stored with a fixed
number of bytes.
It uses a sort of scientific notation: some bits are used to store a fraction,
some store an exponent, and one stores its sign (positive or negative).
Its value is then calculated with the equation:

{% include mathjax.html %}

$$ value = -1^{sign\ bit} \times fraction \times 2^{exponent} $$

This scheme is called _floating-point_.

### Booleans

A boolean variable can have one of two values: true or false.
In C++, we declare booleans with `bool`{:.language-c++},
and we use the keywords `true`{:.language-c++} and `false`{:.language-c++}
for their two values:

```c++
bool test = true;
```

As you will see shortly, they're quite useful when we need our program to make
decisions.

## Converting between different types

There are cases where you want to convert between these types.
Converting from an integer to a floating-point value is as easy as assigning
one to the other:

```c++
int i = 25;
double d = i;
// d is now 25.0
```

Converting from a floating-point value back to an int is just as simple:

```c++
double d = 6.7;
int i = d;
```

but there's a wrinkle.
C++ doesn't do any rounding---it _truncates_ the value by removing the fractional part.
After these two lines, `i` equals 6.

Unfortunately, you can't automatically convert between numeric values and
strings. None of these lines will compile:

```c++
int i = "25";
double d = "6.24";
string s = 68;
```

Since strings and numeric types are fundamentally different,
you need to call functions designed to convert between them.

## Casts

While the compiler will perform some conversions for you, they sometimes don't
make sense! Consider:

```c++
int i = 42.3;
```

This is probably a mistake.
Did I mean to make `i` a `double`{:.language-cpp}?
And if not, why would I give it a non-integer value?
You can have the compiler warn you about situations like this by adding the
`-Wconversion` ("warn about conversions") flag.
If I compile

```c++
#include <iostream>

using namespace std;

int main()
{
    int i = 42.3;
    cout << "i: " << i << endl;
    return 0;
}
```

with `clang++ -std=c++14 -Wall -Wextra -Wconversion -o conversion conversion.cpp`,
I get the following warning:

```
 warning: implicit conversion from 'double' to 'int' changes value from 42.3 to 42
      [-Wliteral-conversion]
    int i = 42.3;
        ~   ^~~~
```

But sometimes you *want* the converted value.
In this case, you can tell the compiler by using a *cast*:

```c++
#include <iostream>

using namespace std;

int main()
{
    double d = 42.3;
    // Casts d to an int.
    // Yes, this is a lot of punctuation - just know that the type you want
    // goes inside the < and >.
    int i = static_cast<int>(d);
    cout << "The integer version of" << d << " is " <<  i << endl;
    return 0;
}
```

By using the `static_cast`{:.language-cpp}, you tell the compiler you want the conversion,
and the program will compile without error, even with `-Wconversion`.

{% include extra.md %}

We'll conclude most lessons with this section,
which explains a bit more about what's going on.
Nothing here is needed to understand the next lesson,
but it's useful to know and might answer some lingering questions.

- It's a good idea to assign variables an initial value
  because C++ usually doesn't give them a default one.
  Thankfully, your compiler will usually warn you if you forget to do so.
  Trying to compile:

  ```c++
  #include <iostream>
  using namespace std;
  int main()
  {
      int foo;
      cout << "foo is " << foo << endl;
      return 0;
  }
  ```

  makes Clang say:

  ```
  warning: variable 'foo' is uninitialized when used here [-Wuninitialized]
        cout << "foo is " << foo << endl;
                           ^~~
  note: initialize the variable 'foo' to silence this warning
        int foo;
               ^
                = 0
  ```

- The name `double`{:.language-cpp} comes from having twice the precision of another
  floating-point type, `float`{:.language-cpp}.
  The former uses eight bytes to store the value, and the latter uses four.
  Since most laptop and desktop processors can perform calculations with up to
  ten bytes of precision, it often makes more sense to use the more precise
  of the two.

- Those familiar with [standard I/O streams](https://en.wikipedia.org/wiki/Standard_streams)
  may wonder how to write to the standard error stream.
  Just like `stdout` is accessible via `cout`,
  you can write to `stderr` with `cerr`.

- We sometimes _do_ care about the size and range of an integer.
  In those cases, we should use

  ```c++
  #include <cstdint>
  ```

  which provides `int8_t`{:.language-cpp}, `int16_t`{:.language-cpp},
  `int32_t`{:.language-cpp}, `int64_t`{:.language-cpp}, and so on.
  The number in the type indicates how many bits are used to store an integer
  of that type.

- You can specify the bit patterns of a variable
  using hexadecimal and binary literals:

  ```cpp
  unsigned int a = 0xff; // FF in base 16, or 255
  unsigned int b = 0b101; // 101 in base 2 (binary), or 5
  ```

  This is useful when interacting directly with hardware,
  or when using a variable to store a collection of single-bit options.
  (However, [other types](http://en.cppreference.com/w/cpp/utility/bitset)
  are probably a better choice for the latter.)

{% include next.md title="Operators" url="operators" %}
