---
layout: page
title: Loops
---

Programs often have to do things several times in a row.
To do so, we use loops:

```cpp
#include <iostream>

using namespace std;

int main()
{
    cout << "Pick a starting  number:" << endl;
    int start;
    cin >> start;

    cout << endl; // Add a blank space between input and output.

    // Count down from the starting value to zero.
    int current = start;
    while (current > 0) {
        cout << current << endl;
        --current;
    }

    return 0;
}
```

## `while` loops

`while (condition)`{:.language-cpp} executes the block that follows it
repeatedly until the condition is no longer true.
In our example above, we loop so long as `current` is a positive value,
causing our program to count down from the given value to zero.

## `do while` loops

In some cases we want to perform an action at least once,
then decide if we want to repeat it:

```cpp
#include <iostream>

using namespace std;

int main()
{
    int theAnswer = 42;
    int guess = 0;

    do {
        cout << "Guess a number: ";
        cin >> guess;

        if (guess > theAnswer) cout << "Too high!" <<endl;
        else if (guess < theAnswer) cout << "Too low!" << endl;

    } while (guess != theAnswer);

    cout << "Correct!" << endl;
    return 0;
}
```

`do { ... } while (condition)`{:.language-cpp} runs its block at least once,
then checks the condition to see if it should repeat.

## `for` loops

In lots of situations, we want to iterate from one value to another,
or perform some action until a condition is met:

```cpp
#include <iostream>

using namespace std;

int main()
{
    cout << "Pick a starting number:" << endl;
    int start;
    cin >> start;

    cout << "Pick an ending number:" << endl;
    int end;
    cin >> end;

    cout << endl; // Add a blank space between input and output.

    if (start < end) {
        for (int current = start; current <= end; ++current) {
            cout << current << endl;
        }
    }
    else if (start > end) {
        for (int current = start; current >= end; --current) {
            cout << current << endl;
        }
    }
    else {
        // We print one and we're done.
        cout << start << endl;
    }

    return 0;
}
```

`for` loops have the form `for (<initialization>; <condition>; <after>)`{:.language-cpp}.
The initialization section is called before the loop starts.
Before each iteration, the condition is checked, and the loop body is only run
if it is true.
Take one of the loops in the previous example:

```cpp
for (int current = start; current >= end; --current) {
    cout << current << endl;
}
```

This has the exact same effect as:

```cpp
{
    int current = start;
    while (current >= end) {
        cout << current << endl;
        --current;
    }
}
```

but it's a bit more succinct.

## `continue` and `break`

We can skip back to the top of a loop using `continue`{:.language-cpp},
and we can exit the loop early with `break`{:.language-cpp}:

```cpp
#include <iostream>

using namespace std;

int main()
{
    int theAnswer = 21;

    while (true) { // Loop endlessly until we break
        cout << "Pick an odd number: ";
        int guess;
        cin >> guess;

        if (guess % 2 != 0) {
            cout << "That's an even number!" << endl;
            continue;
        }

        if (guess > theAnswer) cout << "Too high!" <<endl;
        else if (guess < theAnswer) cout << "Too low!" << endl;
        else break;
    }

    return 0;
}
```

As you've probably noticed, you can write the same loop in many different ways.
Choose whatever seems the clearest in a given situation.
Whatever you do, always make sure your exit conditions are properly
defined---getting caught in an infinite loop is a common programming mistake.
